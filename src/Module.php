<?php

namespace Viktorprogger\VoximplantTest;

use Yii;
use yii\base\InvalidConfigException;
use yii\base\Module as BaseModule;

class Module extends BaseModule
{
    /**
     * @var array Config used to create object via {{@see \Yii::createObject}}
     */
    public $processorConfig = array();

    /**
     * @var ProcessorInterface
     */
    protected $processor;

    /**
     * @throws InvalidConfigException
     */
    public function init()
    {
        parent::init();

        $this->processor = Yii::createObject($this->processorConfig);
        if (! $this->processor instanceof ProcessorInterface) {
            throw new InvalidConfigException('Expression processor must implement \Viktorprogger\VoximplantTest\ProcessorInterface');
        }
    }

    /**
     * @param string $expression
     *
     * @return float
     */
    public function process($expression)
    {
        return $this->processor->calculate($expression);
    }
}
