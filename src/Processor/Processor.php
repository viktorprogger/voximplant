<?php

namespace Viktorprogger\VoximplantTest\Processor;

use Viktorprogger\VoximplantTest\ProcessorInterface;
use yii\base\BaseObject;
use yii\base\InvalidArgumentException;
use yii\base\NotSupportedException;

class Processor extends BaseObject implements ProcessorInterface
{
    /**
     * @var SymbolInterface[]
     */
    public $symbolTypes = array();

    /**
     * @param string $expression
     *
     * @return float|void
     * @throws NotSupportedException
     */
    public function calculate($expression)
    {
        $symbols = $this->parse($expression);
        $expression = $this->buildExpression($symbols);
    }

    /**
     * @param $expression
     *
     * @throws NotSupportedException
     */
    protected function parse($expression)
    {
        $symbols = array();

        for ($i = 0; $i < strlen($expression); $i++) {
            $symbolObject = null;
            foreach ($this->symbolTypes as $symbolType) {
                $symbol = $expression[$i];
                if ($symbolType::match($symbol)) {
                    $symbolObject = new $symbolType($symbol);
                }
            }

            if ($symbolObject === null) {
                throw new NotSupportedException(sprintf("Processor does not support symbol '%s'", $symbol));
            }

            $symbols[] = $symbolObject;
        }

        return $symbols;
    }

    /**
     * @param SymbolInterface[] $symbols
     *
     * @return Expression
     */
    private function buildExpression(array $symbols)
    {
        $expression = new Expression;
        $symbolPrevious = null;

        foreach ($symbols as $symbol) {
            if ($symbol->getType() === AbstractSymbol::TYPE_OPERATOR && $symbolPrevious !== AbstractSymbol::TYPE_OPERAND) {
                throw new InvalidArgumentException('The given expression has errors');
            }
            $symbolPrevious = $symbol->getType();

            switch (true) {
                case ($symbol instanceof DigitalSymbol):
                case ($symbol instanceof DotSymbol):
                    $expression->operandIncrease($symbol->getValue());
                    break;
                case ($symbol instanceof PlusSymbol):
                case ($symbol instanceof MinusSymbol):
                    $expression->addOperator($symbol);
                    break;
                case ($symbol instanceof MultiplicationSymbol):
                case ($symbol instanceof DivisionSymbol):
                    $expression->addOperatorPrioritized($symbol);
                    break;
            }
        }

        return $expression;
    }
}
