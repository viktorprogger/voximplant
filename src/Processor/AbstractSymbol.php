<?php

namespace Viktorprogger\VoximplantTest\Processor;

abstract class AbstractSymbol implements SymbolInterface
{
    const TYPE_OPERAND = 1;
    const TYPE_OPERATOR = 2;

    protected $value;

    public function __construct($value)
    {
        $this->value = $value;
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->value;
    }
}
