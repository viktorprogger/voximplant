<?php

namespace Viktorprogger\VoximplantTest\Processor;

class DigitalSymbol extends AbstractSymbol implements SymbolInterface
{
    /**
     * @param string $symbol
     *
     * @return bool
     */
    public static function match($symbol)
    {
        return is_numeric($symbol);
    }

    /**
     * @return int
     */
    public function getType()
    {
        return self::TYPE_OPERAND;
    }
}
