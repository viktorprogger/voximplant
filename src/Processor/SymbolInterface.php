<?php

namespace Viktorprogger\VoximplantTest\Processor;

interface SymbolInterface
{
    public function __construct($symbol);

    /**
     * @param string $symbol
     *
     * @return bool
     */
    public static function match($symbol);

    /**
     * @return mixed
     */
    public function getValue();

    /**
     * @return int
     */
    public function getType();
}
