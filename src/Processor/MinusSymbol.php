<?php

namespace Viktorprogger\VoximplantTest\Processor;

class MinusSymbol extends AbstractSymbol implements SymbolInterface
{
    /**
     * @param string $symbol
     *
     * @return bool
     */
    public static function match($symbol)
    {
        return '-' === $symbol;
    }

    /**
     * @return int
     */
    public function getType()
    {
        return self::TYPE_OPERATOR;
    }
}
