<?php

namespace Viktorprogger\VoximplantTest\Processor;

class DotSymbol extends AbstractSymbol implements SymbolInterface
{
    /**
     * @param string $symbol
     *
     * @return bool
     */
    public static function match($symbol)
    {
        return $symbol === '.';
    }

    /**
     * @return int
     */
    public function getType()
    {
        return self::TYPE_OPERAND;
    }
}
