<?php

namespace Viktorprogger\VoximplantTest\Processor;

use yii\base\InvalidArgumentException;

class Expression
{
    const STATE_INCOMPLETE = 0;
    const STATE_OK = 1;

    private $state = self::STATE_INCOMPLETE;
    private $nodes = array();

    public function operandIncrease($char)
    {
        $lastNode = array_pop($this->nodes);
        if ($lastNode === null) {
            $lastNode = '';
        }

        if ($lastNode instanceof SymbolInterface) {
            $this->nodes[] = $lastNode;
            $lastNode = '';
        }

        if ($lastNode instanceof self) {
            $lastNode->operandIncrease($char);
        } else {
            $lastNode .= $char;
            $this->nodes[] = $lastNode;
            $this->state = self::STATE_OK;
        }
    }

    public function addOperator(SymbolInterface $symbol)
    {
        if ($this->state === self::STATE_INCOMPLETE) {
            throw new InvalidArgumentException('The given expression has errors');
        }

        $this->nodes[] = $symbol;
        $this->state = self::STATE_INCOMPLETE;
    }

    public function addOperatorPrioritized(SymbolInterface $symbol = null)
    {
        if ($this->state === self::STATE_INCOMPLETE) {
            throw new InvalidArgumentException('The given expression has errors');
        }

        $expression = new self();

        if ($symbol !== null) {
            $lastNode = array_pop($this->nodes);
            $expression->operandIncrease($lastNode);
            $expression->addOperator($symbol);
        }

        $this->nodes[] = $expression;
        $this->state = self::STATE_OK;
    }

    public function subExpressionOpen()
    {
        $lastNode = array_pop($this->nodes);
        if ($lastNode instanceof self) {
            $this->nodes[] = $lastNode;
            $lastNode->subExpressionOpen();
        }

        $this->nodes[] = new self;
    }

    public function subExpressionClose()
    {

        $lastNode = array_pop($this->nodes);
        if ($lastNode instanceof self) {
            $this->nodes[] = $lastNode;
            $lastNode->subExpressionClose();
        }

        // TODO
    }
}
