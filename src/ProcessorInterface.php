<?php

namespace Viktorprogger\VoximplantTest;

interface ProcessorInterface
{
    /**
     * @param string $expression
     * @return float
     */
    public function calculate($expression);
}
